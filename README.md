Instructions for using this extremely hacky approach to keeping Drutopia
projects Drupal.org mirrors up-to-date:

```
git clone git@gitlab.com:drutopia/hackysync.git
cd hackysync
./get_all_repositories.sh
./update_drupal_org_from_gitlab.sh
```

And thereafter:

```
git pull
./update_drupal_org_from_gitlab.sh
```

New projects can be added to `drutopia_projects.txt` (the
`get_all_repositories.sh` script is not
smart enough to only try to add new projects, but if you ignore the
errors it should work).


## Tagging

To push a tag to GitLab and drupal.org use the script
`tag_project_release.sh`. Sample command:

```
./tag_project_release.sh drutopia_article 1.0-alpha1
```

This will pull from GitLab before tagging, and create the 8.x-prefixed tags as well.

For non-Drutopia projects, change directory first, e.g. `cd agaric/drupal/ && ../../tag_project_release.sh twigsuggest 1.0-beta1`

## Release notes

See [`commits-since-most-recent-tag.sh`](commits-since-most-recent-tag.sh) for a little help auto-generating release notes from the commit history.
